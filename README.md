# Project With Local Disabled Rule

This project disables one of the `kics` rules in its local `.gitlab/sast-ruleset.toml` file.